package com.experimental;

import com.giffing.wicket.spring.boot.context.scan.WicketHomePage;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

@WicketHomePage
@AuthorizeInstantiation("USER")
@MountPath("/dashboard")
public class Dashboard extends BasePage
{
    public Dashboard(PageParameters parameters)
    {
        super(parameters);
    }
}
