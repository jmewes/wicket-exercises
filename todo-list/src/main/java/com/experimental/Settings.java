package com.experimental;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath("/settings")
@AuthorizeInstantiation("USER")
public class Settings extends BasePage
{
    public Settings(final PageParameters parameters)
    {
        super(parameters);
    }
}
