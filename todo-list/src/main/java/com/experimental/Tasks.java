package com.experimental;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.wicketstuff.annotation.mount.MountPath;

@MountPath("/tasks")
@AuthorizeInstantiation("USER")
public class Tasks extends BasePage
{
    public Tasks(PageParameters parameters)
    {
        super(parameters);
    }
}
