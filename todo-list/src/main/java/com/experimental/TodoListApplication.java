package com.experimental;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class TodoListApplication
{
    public static void main(String[] args)
    {
        new SpringApplicationBuilder().sources(TodoListApplication.class).run(args);
    }
}
